# Momentus Javascript SDK

The provided tool kit gives methods to plan Momentus Rides in space. This toolkit can be added
to any webpage by including the following line to the <head> tag of the html document.

```
<script src="http://api.momentus.space/momentus-sdk-v0.7.7.js"></script>
```

## Making a Momentus Request

Requests may be made from the Momentus `Ride` module to be plan ferried trips 
between extraterrestrial `origins` and `destinations`. Launch information can also 
be added for additional information.

To make a request to either module, a "request" object should be made as follows:

```
var request = {
  payload: {
    mass: 200,                            // kg 
    height: 0,                            // cm
    diameter: 0                           // cm
  },
  includeLaunch: true,                    // whether the launch vehicle should be included, or not. boolean
  initialOrbitMode: Momentus.Mode.LEO,    // see 'Mode Contants'
  finalOrbitMode:   Momentus.Mode.LEO,    // see 'Mode Contants'
  origin: {                               // Momentus.Location
    perigee:        300,
    apogee:         300,
    inclination:    30 ,
    raan:           0  ,
    omega:          0  ,
    ssoAltitude:    500,
    LTAN_hr:        9,  
    LTAN_min:       30  
  },
  destination: {                          // Momentus.Location
    perigee: 650,   
    apogee:  650,   
    inclination: 50,
    rann: 0,  
    omega: 0,                             // periapsis
    ssoAltitude: 600,   
    LTAN_hr: 9,                           // (optional) Time, hours
    LTAN_min: 50,                         // (optional) Time, minutes
    lunarAltitude: 200,
    C3: 3
  }
}

```

### Mode Contants

You may use the following constants to configure your ride's orbits.


* Momentus.Mode.EARTH
* Momentus.Mode.LEO
* Momentus.Mode.SSO
* Momentus.Mode.LUNAR
* Momentus.Mode.ESCAPE

### Points, or Places

Momentus requests have two points, or places defined as: `origin` and `destination`. These
have many attributes, some of which may be optional.


| Name                         | Variable Name  | Notes                    |
| ---------------------------- | -------------- | ------------------------ |
| Perigee Altitude             | perigee        | alt_peri_ini             |
| Apogee Altitude              | apogee         | alt_apo_ini              |
| Inclination                  | inclination    | inc_ini                  |
| Longitude of Ascending Node  | raan           | RAAN_ini                 |
| Argument of Periapsis        | omega          | periapsis                |
| SSO Altitude                 | ssoAltitude    | SSO Mode Only. altSSO    |
| Local Time at Ascending Node | localTime      | SSO Mode Only.           |
| C3                           | C3             | Only on destination      |

## Units

The preferred units are as follows:

* Inclination: Degrees (deg)
* Distance: Kilometers (km)
* Weight: Kilograms (kg)
* Dimensions: Centimeters (cm)
* Time: Seconds (s)


## Models

### Momentus Ride

The `Momentus.RIDES` variable is an array which contains a
list of all Momentus Rides presently available in the API.

A `Momentus Ride` model has the following attributes:

| Name               | method name      | type | Unit  | Description             |
| ------------------ | ---------------- | ---- | ----- | --------------------    |
| Name               | name             | str  |       | e.g. Vigoride           |
| System Dry Mass    | massDry          | int  | kg    | Basic Empty Weight      |
| Propellant         | propellant       | int  | kg    | Fuel Capacity           |
| Max Payload        | payloadMax       | int  | kg    | max size of payload     |
| Delta-V            | dV               | int  |       |                         |
| Price              | price            | int  | USD   |                         |
| Propellant         | propellant       | int  |       |                         |
| Leave Behind Mass  | leaveBehind      | int  |       |                         |
| Specific Impulse   | specificImpulse  | int  |       |                         |
| Thrust             | thrust           | int  |       |                         |
| Max Duty           | maxDuty          | int  |       |                         |
| Diameter           | diameter         | int  | cm    |                         |
| Height             | height           | int  | cm    |                         |
| Payload Height Min | heightPayloadMin | int  | cm    |                         |
| Delta-V Penalty    | dVPenalty        | float|       | Factor_DeltaVPenalty    |
| Trip Time Penalty  | tripTimePenalty  | float| Secs  | Factor_TripTimePenalty  |
| Price              | price            | int  | USD   |                         |

### Launchers

The `Momentus.LAUNCHERS` variable is an array which contains a 
list of all the Launch vehicles Momentus integrates with.

A `Launcher` model has the following attributes:

| Name                     | method name        | type | Unit  | Description         |
| ------------------------ | ------------------ | ---- | ----- | ------------------- |
| Name                     | name               | str  |       |                     |
| Launch Provider          | provider           | str  |       |                     |
| Country                  | country            | str  |       |                     |
| Orbit Type               | orbit              | str  |       |                     |
| Perigee                  | perigee            | int  | deg   |                     |
| apogee                   | apogee             | int  | deg   |                     |
| inclinationMax           | inclinationMax     | int  | deg   |                     |
| inclinationMin           | inclinationMin     | int  | deg   |                     |
| longitudeOfAscendingNode | longitudeOfAscendingNode | int | deg |                  |
| argumentOfPeriapsis      | argumentOfPeriapsis| int  | deg   |                     | 
| Max. Incl. Launch Mass   | massMax            | int  | kg    |                     |
| Min. Incl. Launch Mass   | massMin            | int  | kg    |                     |
| Payload Diameter         | payloadDiameter    | int  | cm    |                     |
| Payload Height           | payloadHeight      | int  | cm    |                     |
| Price                    | price              | int  | USD   |                     |


#### Example: Print All Momentus Rides

```
Momentus.RIDES.forEach(function(ride) {
    print(ride.name);
});

```

### Momentus Trip

A `Momentus Trip` contains estimations for a proposed mission.  A Trip has the following attributes:

| Name                 | attribute name    | returns | Unit    | Description                      |
| -------------------- | ----------------- | ------- | ------- | -------------------------------- |
| Valid                |  valid            | bool    | N/A     | Is this ride potentially viable  |
| Error                |  error            | str     | N/A     | If not viable, error in english  |
| Exhaust Speed        |  vel_exhaust      | numeric |         |                                  |
| Mass Leave Behind    |  mass_leavebehind | numeric |         |                                  |
| Wet Mass             |  massWet          | numeric |         |                                  |
| Launch Mass          |  massLaunch       | numeric |         |                                  |
| Initial Mass         |  massInitial      | numeric |         |                                  |
| Delta-V Available    |  dVavailable      | numeric |         |                                  |
| Delta-V Needed       |  dVneed           | numeric |         |                                  |
| Delta-V Remain       |  dVremain         | numeric |         |                                  |
| Delta-V Remain %     |  dVremainPer      | numeric | %       |                                  |
| Duration of Ride     |  duration         | int     | seconds |                                  |
| Launcher Price       |  priceLaunch      | numeric | USD     |                                  |
| Momentus Ride Price  |  priceRide        | numeric | USD     |                                  |
| Total Price          |  price            | numeric | USD     |                                  |


## Momentus Calculations

The `Momentus` class is available to return calculations and enumerate available rides.

### `Momentus.Ride` Methods

The `Momentus.Ride` class has the following static methods:

| method name                              | returns | notes                                                                            |
| ---------------------------------------- | ------- | -------------------------------------------------------------------------------- |
| planTrip(request, ride [, launch]);      | Trip    | launch parameter is optional, but is required to validate a launcher with a ride | 
|----------------------------------------- | ------- | -------------------------------------------------------------------------------- |

A `planTrip` call, may look like this:

```
var ride = Momentus.RIDES[0];
var request = {                 // see above for a full request
  payload: {
    mass: 123,                  // kg 
    diameter: 1,                // cm
    height: 123,                // cm
  },
  origin: { ... },              // Momentus.Location
  destination: { ... }          // Momentus.Location
}

var trip = Momentus.planTrip(request, ride);
=> <Trip ride=...>

```

## Version bumping

* webpack.config.js
* package.json
